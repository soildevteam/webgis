package org.iransoil.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "role", schema = "public")
public class Role {
	
	@Id
	@SequenceGenerator(sequenceName = "role_id_seq", name = "RoleIdSequence", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "RoleIdSequence")
	private Long id;

	private String name;

	@ManyToMany(mappedBy = "roles")
	private Set<Account> users;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Account> getUsers() {
		return users;
	}

	public void setUsers(Set<Account> users) {
		this.users = users;
	}
}
