package org.iransoil.enums;

public enum RoleType {

	ADMINISTRATOR(1L), USER(2L);

	private Long roleId;

	private RoleType(Long roleId) {
		this.roleId = roleId;
	}

	public Long getRoleId() {
		return roleId;
	}

}
