package org.iransoil.service;

import org.iransoil.model.Account;

public interface AccountService {
    void save(Account user);

    Account findByUsername(String username);
}
