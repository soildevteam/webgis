package org.iransoil.service;

import java.util.HashSet;
import java.util.Set;

import org.iransoil.enums.RoleType;
import org.iransoil.model.Account;
import org.iransoil.model.Role;
import org.iransoil.repository.AccountRepository;
import org.iransoil.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {
	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public void save(Account account) {
		account.setPassword(bCryptPasswordEncoder.encode(account.getPassword()));
		Role userRole = roleRepository.findOne(RoleType.USER.getRoleId());
		Set<Role> roles = new HashSet<>();
		roles.add(userRole);
		account.setRoles(roles);
		accountRepository.save(account);
	}

	@Override
	public Account findByUsername(String username) {
		return accountRepository.findByUsername(username);
	}
}
